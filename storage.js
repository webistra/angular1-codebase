/**
 * Created by Mobiloitte on 16/10/2017.
 */

'use strict';

angular.module('Lawcunae.storage',[])

.service('storage',function () {

    this.set = function (key,value) {
        localStorage.setItem(key, JSON.stringify(value));
    }
    this.get = function (key) {
        try {
            return JSON.parse(localStorage.getItem(key));
        } catch (e) {
            return null;
        }
    }
    this.remove = function (key) {
        alert(key)
        localStorage.removeItem(key);
    }
    
})

.service('commonService',function ($window) {
    var self=this;
    self.browser_id=new Fingerprint().get();
    self.user_id=null;
    self.user_info=null;
    self.review_card_arr=[];
    self.stateChangeVal='';
    self.tabReloadStatus=false;
    self.allTabsContent={search:[], document:[], case:[], graph:[], profile:[], article:[]};
    
    self.ToShowAlert = function(msg) {
        window.alert(msg);
    };

    self.showLoader = function(msg) {
        $('#loader').show();
    };

    self.hideLoader = function(msg) {
        $('#loader').hide();
    };
})